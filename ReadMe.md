# Housing Loan Calculator Application

## Introduction
This Java application is a Spring Boot-based housing loan calculator that allows users to calculate the monthly repayment plan based on a series loan principle with a fixed interest rate. It features a simple web interface where users can enter the desired loan amount and the payback time in years.

## Features
- Calculation of monthly repayments for housing loans.
- Fixed interest rate of 3.5% for housing loans.
- Extendable for different loan types (e.g., car loans, personal loans) with different interest rates.
- Web-based user interface for inputting loan details and viewing repayment plan.

## Prerequisites
- Java 17
- Maven 3.6 or later

## Installation
To set up the project on your local machine, follow these steps:

1. Clone the repository:
   ```sh
   git clone https://github.com/your-username/your-repo-name.git

2. Build the project with Maven:
   ```sh
   mvn clean install
3. To run the application:
   ```sh
   mvn spring-boot:run
4. Access the web interface at
   ```sh
   http://localhost:8080/