package com.calculation.loan.controller;

import com.calculation.loan.model.Loan;
import com.calculation.loan.model.LoanType;
import com.calculation.loan.service.LoanService;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
@AllArgsConstructor
public class LoanController {


    private final LoanService loanService;

    @GetMapping("/")
    public String home(){
        return "home";
    }

    @PostMapping("/calculate")
    public String calculateLoan(
            @RequestParam double amount,
            @RequestParam int years,
            @RequestParam LoanType loanType,
            Model model) {
        Loan loan = loanService.calculateLoan(amount, years, loanType);
        model.addAttribute("loan", loan);
        return "result";
    }
}
