package com.calculation.loan.model;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum LoanType {
    HOUSE_LOAN(3.5, "Housing Loan (3.5%)"),
    CAR_LOAN(6.0, "Car Loan (6%)"),
    SPENDING_LOAN(21.0, "Spending Loan (21%)");

    private final double interestRate;
    private final String value;

}
