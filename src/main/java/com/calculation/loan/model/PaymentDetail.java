package com.calculation.loan.model;

import lombok.Builder;
import lombok.Data;

@Builder
@Data
public class PaymentDetail {
    private int month;
    private String principalPaid;
    private String interestPaid;
    private String remainingBalance;
    private String monthlyPayment;
}
