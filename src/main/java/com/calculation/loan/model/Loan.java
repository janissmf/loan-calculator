package com.calculation.loan.model;

import lombok.Builder;
import lombok.Data;

import java.util.List;

@Builder
@Data
public class Loan {
    private double amount;
    private int years;
    private double monthlyPayment;
    private String loanTypeValue;
    private List<PaymentDetail> paymentDetails;
}
