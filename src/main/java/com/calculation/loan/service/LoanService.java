package com.calculation.loan.service;

import com.calculation.loan.model.Loan;
import com.calculation.loan.model.LoanType;
import com.calculation.loan.model.PaymentDetail;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.text.NumberFormat;
import java.util.List;
import java.util.concurrent.atomic.AtomicReference;
import java.util.stream.IntStream;

@Service
@AllArgsConstructor
public class LoanService {

    private final NumberFormat euroNumberFormat;

    public Loan calculateLoan(double amount, int years, LoanType loanType) {
        double rate = loanType.getInterestRate();
        int months = years * 12;
        double monthlyRate = rate / 1200;
        double monthlyPayment = (amount * monthlyRate) / (1 - Math.pow(1 + monthlyRate, -months));
        AtomicReference<Double> amountCalc = new AtomicReference<>(amount);

        List<PaymentDetail> paymentDetails = IntStream.rangeClosed(1, months)
                .mapToObj(month -> {
                    double interestPaid = amountCalc.get() * monthlyRate;
                    double principalPaid = monthlyPayment - interestPaid;
                    amountCalc.updateAndGet(v -> v - principalPaid);

                    return PaymentDetail.builder()
                            .month(month)
                            .principalPaid(euroNumberFormat.format(principalPaid))
                            .interestPaid(euroNumberFormat.format(interestPaid))
                            .remainingBalance(euroNumberFormat.format(amountCalc.get().doubleValue()))
                            .monthlyPayment(euroNumberFormat.format(principalPaid + interestPaid))
                            .build();
                })
                .toList();

        return Loan.builder()
                .amount(amount)
                .years(years)
                .monthlyPayment(monthlyPayment)
                .paymentDetails(paymentDetails)
                .loanTypeValue(loanType.getValue())
                .build();
    }
}
