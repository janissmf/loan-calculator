package com.calculation.loan.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.text.NumberFormat;
import java.util.Locale;

@Configuration
public class ThymeleafConfig {

    @Bean(name = "euroNumberFormat")
    public NumberFormat euroNumberFormat() {
        return NumberFormat.getCurrencyInstance(Locale.GERMANY); // A locale that uses the Euro
    }
}
