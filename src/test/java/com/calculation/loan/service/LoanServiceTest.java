package com.calculation.loan.service;

import com.calculation.loan.model.Loan;
import com.calculation.loan.model.LoanType;
import com.calculation.loan.model.PaymentDetail;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Spy;
import org.mockito.junit.jupiter.MockitoExtension;

import java.text.NumberFormat;
import java.util.List;
import java.util.Locale;

import static org.junit.jupiter.api.Assertions.assertNotNull;

@ExtendWith(MockitoExtension.class)
class LoanServiceTest {

    @InjectMocks
    LoanService loanService;

    @Spy
    NumberFormat euroNumberFormat = NumberFormat.getCurrencyInstance(Locale.GERMANY);


    @Test
    void testCalculateLoan() {

        double amount = 10000.0;
        int years = 5;
        double expectedMonthlyPayment = 193.33;
        LoanType loanType = LoanType.CAR_LOAN;

        Loan loan = loanService.calculateLoan(amount, years, loanType);

        Assertions.assertEquals(amount, loan.getAmount());
        Assertions.assertEquals(years, loan.getYears());
        Assertions.assertEquals(expectedMonthlyPayment, loan.getMonthlyPayment(), 0.01);
        Assertions.assertEquals(loanType.getValue(), loan.getLoanTypeValue());

        List<PaymentDetail> paymentDetails = loan.getPaymentDetails();
        Assertions.assertEquals(years * 12, paymentDetails.size());

        double remainingBalance = amount;
        for (int month = 1; month <= years * 12; month++) {
            PaymentDetail paymentDetail = paymentDetails.get(month - 1);

            double interestPaid = remainingBalance * loanType.getInterestRate() / 1200;
            double principalPaid = expectedMonthlyPayment - interestPaid;
            remainingBalance -= principalPaid;

            Assertions.assertEquals(month, paymentDetail.getMonth());
            assertNotNull(paymentDetail.getPrincipalPaid());
            assertNotNull(paymentDetail.getInterestPaid());
            assertNotNull(paymentDetail.getRemainingBalance());
            assertNotNull(paymentDetail.getMonthlyPayment());
        }
    }
}