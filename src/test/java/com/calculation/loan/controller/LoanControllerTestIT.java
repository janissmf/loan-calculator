package com.calculation.loan.controller;

import com.calculation.loan.model.LoanType;
import org.hamcrest.Matchers;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
@SpringBootTest
@AutoConfigureMockMvc
class LoanControllerTestIT {

    @Autowired
    private MockMvc mockMvc;

    @Test
    void testHome() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/")
                        .contentType(MediaType.TEXT_HTML))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.view().name("home"));
    }

    @Test
    void testCalculateLoan() throws Exception {
        double amount = 10000.0;
        int years = 5;
        LoanType loanType = LoanType.CAR_LOAN;

        mockMvc.perform(MockMvcRequestBuilders.post("/calculate")
                        .param("amount", String.valueOf(amount))
                        .param("years", String.valueOf(years))
                        .param("loanType", loanType.toString())
                        .contentType(MediaType.APPLICATION_FORM_URLENCODED))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.view().name("result"))
                .andExpect(MockMvcResultMatchers.model().attributeExists("loan"))
                .andExpect(MockMvcResultMatchers.model().attribute("loan", Matchers.hasProperty("amount", Matchers.equalTo(amount))))
                .andExpect(MockMvcResultMatchers.model().attribute("loan", Matchers.hasProperty("years", Matchers.equalTo(years))))
                .andExpect(MockMvcResultMatchers.model().attribute("loan", Matchers.hasProperty("loanTypeValue", Matchers.equalTo(loanType.getValue()))));
    }
}